---
title: Virtual Consular Office
type: page
---

The consular office may be access via any Minecraft: Java Edition client by
connecting to <strong>mc.consulate.scot</strong>. Please ask in our IRC channel
(<a href="ircs://chat.freenode.net:6697/scottishconsulate">#scottishconsulate</a>
on Freenode) to be added to the allow list.

<img src="https://mc.ggservers.com/status/3624.png" style="width:100%;max-width:500px;">
