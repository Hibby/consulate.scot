---
title: RSGB SSB Field Day 2020
published: 2020-09-05
---

<img src="/nfdssb2020-tent.jpg" style="width:100%;max-width:1000px;">

This year's SSB Field Day contest was held on the 5th and 6th of September and
ran from 1300 to 1300 UTC. MS0SCZ is not an RSGB-affiliated society (we don't
want to handle money at all to keep it simple) and the contest was changed to
single-operator only due to the ongoing global pandemic. For these reasons we
did not officially enter the contest, but we still played along for a day
anyway.

We gathered at a location not far from 57North Hacklab and established a small
camp. The main feature was Ed MM6MVP's tent with enough room to stand in,
furnished with a table and some benches. On the table, Hibby 2M0HIB's Yaesu
FT-891 and Ed's leisure battery.

<img src="/nfdssb2020-battery.jpg" style="width:100%;max-width:1000px;">

With the antenna erected and the station set up, we began making contacts. In
addition to Ed and Hibby, Ana MM0YSO and Iain MM0ROR also operated the station
to make a total of 19 contacts by the end of the day. We have uploaded the log
to QRZ.com and also uploaded to the RSGB as a checklog.

<img src="/nfdssb2020-radio.jpg" style="width:100%;max-width:1000px;">

To keep ourselves nourished throughout the event, Iain also operated a BBQ to
produce burgers for the operators, and they were tasty.
