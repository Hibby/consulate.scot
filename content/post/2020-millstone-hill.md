---
title: Millstone Hill SOTA Activation
published: 2020-09-27
---

<img src="/millstone2020-peak.jpg" style="width:100%;max-width:1000px;">

Starting from the Donview car park on the minor road along the north side of
the River Don, just south of Bennachie, four members of the Scottish Consulate
Amateur Radio Club began their climb of Millstone Hill. A 320-meter ascent to
the peak, to reach 410 meters above sea level.

<img src="/millstone2020-x1m.jpg" style="width:100%;max-width:1000px;">

48 or so minutes later everyone had arrived at the top and began to set up
radio stations. Ed MM6MVP had his X1M although unfortunately, the antenna had
broken. Iain MM0ROR made the first contact via the GB3GN repeater in Banchory
with 2M0DOI but being via a repeater didn't qualify for SOTA.

<img src="/millstone2020-hf.jpg" style="width:100%;max-width:1000px;">

During this time Hibby 2M0HIB had been busy getting his mast and dipole up and
had found a small shelter to hide out of the wind. He had already activated
this peak earlier in the year, so Ed moved over to this station and worked 5
contacts, enough to count for SOTA points.

<img src="/millstone2020-shelter.jpg" style="width:100%;max-width:1000px;">

Now that we were all sufficiently chilled from the wind, it was time to head
back down to the car park.

<img src="/millstone2020-return.jpg" style="width:100%;max-width:1000px;">
