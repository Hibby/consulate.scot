---
title: Code of Conduct
type: page
---

We are committed to making the Scottish Consulate an inclusive and welcoming
group for everyone.

Everyone who visits the Scottish Consulate, digitally or in person, is required
to abide by this code of conduct. We will not tolerate harassment of members
and participants or discriminatory behaviour of any form. Specifically:

  * Do not engage in homophobic, racist, transphobic, ableist, sexist, or
    otherwise prejudiced behaviour.
  * Do not harass people. Stalking, unconsented physical contact, or sexual
    attention is harassment. Dressing or acting in a certain way is not
    consent.
  * Every participant's personal space is their own. If you are asked to leave
    someone alone you must respect this.
  * Some members may not want to be filmed or photographed. Respect their wishes.
  * Aggression, elitism and 'well actually' behaviour are not welcome - we are
    trying to foster a community where nobody should be afraid to ask
    questions.

If you break these rules, we may temporarily ask you to leave, and we also
reserve the right to ban you from future events.

If you are being harassed or witness a breach of this code of conduct, you can
get in touch with us in the following ways:

* Find hibby or [tj], perhaps in IRC, and let them know.

### Suggestions

We're still working this out, and suggestions on both COC terms & reporting
methods are welcome!
